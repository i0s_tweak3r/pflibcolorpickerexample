# README #

A tweak that demonstrates [libcolopicker](https://bitbucket.org/pixelfiredev/libcolorpicker/overview) the library that makes it easy to add user customizable colors to your tweak. Works on iOS 6.x.x, 7.x.x & 8.x.x