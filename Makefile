ARCHS = armv7 arm64

TARGET = iphone:clang:latest:6.0

include theos/makefiles/common.mk

TWEAK_NAME = PFLibColorPickerExample
PFLibColorPickerExample_CFLAGS = -fno-objc-arc
PFLibColorPickerExample_FILES = PFLibColorPickerExample.xm PSPDFModernizer.m
PFLibColorPickerExample_FRAMEWORKS = Foundation UIKit
PFLibColorPickerExample_LIBRARIES = colorpicker

include $(THEOS_MAKE_PATH)/tweak.mk

after-install::
	install.exec "killall -9 backboardd"
SUBPROJECTS += pflibcolorpickerexampleprefs
include $(THEOS_MAKE_PATH)/aggregate.mk
