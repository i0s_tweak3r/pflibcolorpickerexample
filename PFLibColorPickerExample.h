//
//  PFLibColorPickerExample.x
//  PFLibColorPickerExample
//
//  Created by rob311 on 06.07.2015.
//  Copyright (c) 2015 rob311. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

static NSString *const kPFLibColorPickerExampleSettings = @"/var/mobile/Library/Preferences/com.rob311.pflibcolorpickerexample.plist";
static NSMutableDictionary *settings;
